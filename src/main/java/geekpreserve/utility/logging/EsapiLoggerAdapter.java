/*
 * Copyright (c) 2017 Billy Flanagan
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package geekpreserve.utility.logging;

import org.owasp.esapi.Logger;
import org.slf4j.Marker;
import org.slf4j.event.Level;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MarkerIgnoringBase;
import org.slf4j.helpers.MessageFormatter;
import org.slf4j.spi.LocationAwareLogger;

import java.io.Serializable;

import static geekpreserve.utility.logging.Validations.notNull;
import static org.owasp.esapi.Logger.EVENT_UNSPECIFIED;
import static org.slf4j.spi.LocationAwareLogger.*;

public class EsapiLoggerAdapter extends MarkerIgnoringBase implements LocationAwareLogger, Serializable {

    private static final long serialVersionUID = 4593289278296716309L;

    private final Logger esapiLogger;
    private final String name;

    public EsapiLoggerAdapter(Logger esapiLogger, String name) {
        notNull(esapiLogger, "esapiLogger cannot be null");
        Validations.notNullOrEmpty(name, "name cannot be null or empty");
        this.esapiLogger = esapiLogger;
        this.name = name;
    }

    @Override
    public void log(Marker marker, String fqcn, int level, String message, Object[] argArray, Throwable t) {
        // the slf4j samples I read all ignore the argArray, and pass through the message and throwable without alteration.
        // not wild about this, but I'll do it their way until I can dig a little more.
        switch (level) {
            case TRACE_INT:
                esapiLogger.trace(EVENT_UNSPECIFIED, message, t);
                return;
            case DEBUG_INT:
                esapiLogger.debug(EVENT_UNSPECIFIED, message, t);
                return;
            case INFO_INT:
                esapiLogger.info(EVENT_UNSPECIFIED, message, t);
                return;
            case WARN_INT:
                esapiLogger.warning(EVENT_UNSPECIFIED, message, t);
                return;
            case ERROR_INT:
                esapiLogger.error(EVENT_UNSPECIFIED, message, t);
                return;
            default:
                throw new IllegalArgumentException("Unknown log level number: " + level);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isTraceEnabled() {
        return esapiLogger.isTraceEnabled();
    }

    @Override
    public void trace(String s) {
        esapiLogger.trace(EVENT_UNSPECIFIED, s);
    }

    @Override
    public void trace(String s, Object o) {
        if (isTraceEnabled()) {
            FormattingTuple ft = MessageFormatter.format(s, o);
            esapiLogger.trace(EVENT_UNSPECIFIED, ft.getMessage(), ft.getThrowable());
        }
    }

    @Override
    public void trace(String s, Object o, Object o1) {
        if (isTraceEnabled()) {
            FormattingTuple ft = MessageFormatter.format(s, o, o1);
            esapiLogger.trace(EVENT_UNSPECIFIED, ft.getMessage(), ft.getThrowable());
        }
    }

    @Override
    public void trace(String s, Object... objects) {
        if (isTraceEnabled()) {
            FormattingTuple ft = MessageFormatter.arrayFormat(s, objects);
            esapiLogger.trace(EVENT_UNSPECIFIED, ft.getMessage(), ft.getThrowable());
        }
    }

    @Override
    public void trace(String s, Throwable throwable) {
        if (isTraceEnabled()) {
            esapiLogger.trace(EVENT_UNSPECIFIED, s, throwable);
        }
    }

    @Override
    public boolean isDebugEnabled() {
        return esapiLogger.isDebugEnabled();
    }

    @Override
    public void debug(String s) {
        esapiLogger.debug(EVENT_UNSPECIFIED, s);
    }

    @Override
    public void debug(String s, Object o) {
        if (isDebugEnabled()) {
            FormattingTuple ft = MessageFormatter.format(s, o);
            esapiLogger.debug(EVENT_UNSPECIFIED, ft.getMessage(), ft.getThrowable());
        }
    }

    @Override
    public void debug(String s, Object o, Object o1) {
        if (isDebugEnabled()) {
            FormattingTuple ft = MessageFormatter.format(s, o, o1);
            esapiLogger.debug(EVENT_UNSPECIFIED, ft.getMessage(), ft.getThrowable());
        }
    }

    @Override
    public void debug(String s, Object... objects) {
        if (isDebugEnabled()) {
            FormattingTuple ft = MessageFormatter.arrayFormat(s, objects);
            esapiLogger.debug(EVENT_UNSPECIFIED, ft.getMessage(), ft.getThrowable());
        }
    }

    @Override
    public void debug(String s, Throwable throwable) {
        if (isDebugEnabled()) {
            esapiLogger.debug(EVENT_UNSPECIFIED, s, throwable);
        }
    }

    @Override
    public boolean isInfoEnabled() {
        return esapiLogger.isInfoEnabled();
    }

    @Override
    public void info(String s) {
        esapiLogger.info(EVENT_UNSPECIFIED, s);
    }

    @Override
    public void info(String s, Object o) {
        if (isInfoEnabled()) {
            FormattingTuple ft = MessageFormatter.format(s, o);
            esapiLogger.info(EVENT_UNSPECIFIED, ft.getMessage(), ft.getThrowable());
        }
    }

    @Override
    public void info(String s, Object o, Object o1) {
        if (isInfoEnabled()) {
            FormattingTuple ft = MessageFormatter.format(s, o, o1);
            esapiLogger.info(EVENT_UNSPECIFIED, ft.getMessage(), ft.getThrowable());
        }
    }

    @Override
    public void info(String s, Object... objects) {
        if (isInfoEnabled()) {
            FormattingTuple ft = MessageFormatter.arrayFormat(s, objects);
            esapiLogger.info(EVENT_UNSPECIFIED, ft.getMessage(), ft.getThrowable());
        }
    }

    @Override
    public void info(String s, Throwable throwable) {
        if (isInfoEnabled()) {
            esapiLogger.info(EVENT_UNSPECIFIED, s, throwable);
        }
    }

    @Override
    public boolean isWarnEnabled() {
        return esapiLogger.isWarningEnabled();
    }

    @Override
    public void warn(String s) {
        esapiLogger.warning(EVENT_UNSPECIFIED, s);
    }

    @Override
    public void warn(String s, Object o) {
        if (isWarnEnabled()) {
            FormattingTuple ft = MessageFormatter.format(s, o);
            esapiLogger.warning(EVENT_UNSPECIFIED, ft.getMessage(), ft.getThrowable());
        }
    }

    @Override
    public void warn(String s, Object... objects) {
        if (isWarnEnabled()) {
            FormattingTuple ft = MessageFormatter.arrayFormat(s, objects);
            esapiLogger.warning(EVENT_UNSPECIFIED, ft.getMessage(), ft.getThrowable());
        }
    }

    @Override
    public void warn(String s, Object o, Object o1) {
        if (isWarnEnabled()) {
            FormattingTuple ft = MessageFormatter.format(s, o, o1);
            esapiLogger.warning(EVENT_UNSPECIFIED, ft.getMessage(), ft.getThrowable());
        }
    }

    @Override
    public void warn(String s, Throwable throwable) {
        if (isWarnEnabled()) {
            esapiLogger.warning(EVENT_UNSPECIFIED, s, throwable);
        }

    }

    @Override
    public boolean isErrorEnabled() {
        return esapiLogger.isErrorEnabled();
    }

    @Override
    public void error(String s) {
        esapiLogger.error(EVENT_UNSPECIFIED, s);
    }

    @Override
    public void error(String s, Object o) {
        if (isErrorEnabled()) {
            FormattingTuple ft = MessageFormatter.format(s, o);
            esapiLogger.error(EVENT_UNSPECIFIED, ft.getMessage(), ft.getThrowable());
        }
    }

    @Override
    public void error(String s, Object o, Object o1) {
        if (isErrorEnabled()) {
            FormattingTuple ft = MessageFormatter.format(s, o, o1);
            esapiLogger.error(EVENT_UNSPECIFIED, ft.getMessage(), ft.getThrowable());
        }
    }

    @Override
    public void error(String s, Object... objects) {
        if (isErrorEnabled()) {
            FormattingTuple ft = MessageFormatter.arrayFormat(s, objects);
            esapiLogger.error(EVENT_UNSPECIFIED, ft.getMessage(), ft.getThrowable());
        }
    }

    @Override
    public void error(String s, Throwable throwable) {
        if (isErrorEnabled()) {
            esapiLogger.error(EVENT_UNSPECIFIED, s, throwable);
        }
    }

}
