/*
 * Copyright (c) 2017 Billy Flanagan
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package geekpreserve.utility.logging;

import org.owasp.esapi.ESAPI;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static geekpreserve.utility.logging.Validations.notNullOrEmpty;

public class EsapiLoggerFactory implements ILoggerFactory {
    private final ConcurrentMap<String, Logger> loggerCache;

    public EsapiLoggerFactory() {
        loggerCache = new ConcurrentHashMap<>();
    }

    public Logger getLogger(String name) {
        notNullOrEmpty(name, "name cannot be null or empty");

        Logger slf4jLogger = loggerCache.get(name);
        if (slf4jLogger != null) {
            return slf4jLogger;
        }
        else {
            Logger newInstance = new EsapiLoggerAdapter(ESAPI.getLogger(name), name);
            Logger oldInstance = loggerCache.putIfAbsent(name, newInstance);
            return oldInstance == null ? newInstance : oldInstance;
        }
    }
}
