/*
 * Copyright (c) 2017 Billy Flanagan
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package geekpreserve.utility.logging;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.owasp.esapi.Logger;
import org.slf4j.Marker;
import org.slf4j.spi.LocationAwareLogger;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.owasp.esapi.Logger.EVENT_UNSPECIFIED;
import static org.slf4j.spi.LocationAwareLogger.*;

public class EsapiLoggerAdapterTests {
    private static final int INVALID_LOG_LEVEL = -1;

    @Rule
    public ExpectedException expected = ExpectedException.none();

    @Mock
    private Logger mockLogger;

    private EsapiLoggerAdapter adapter;

    @Before
    public void before() {
        initMocks(this);
        when(mockLogger.isTraceEnabled()).thenReturn(true);
        when(mockLogger.isDebugEnabled()).thenReturn(true);
        when(mockLogger.isInfoEnabled()).thenReturn(true);
        when(mockLogger.isWarningEnabled()).thenReturn(true);
        when(mockLogger.isErrorEnabled()).thenReturn(true);
        adapter = new EsapiLoggerAdapter(mockLogger, "loggerName");
    }

    @Test
    public void constructorShouldRejectNullEsapiLogger() {
        expected.expect(IllegalArgumentException.class);
        expected.expectMessage("esapiLogger cannot be null");

        new EsapiLoggerAdapter(null, "loggerName");
    }

    @Test
    public void constructorShouldRejectNullName() {
        expected.expect(IllegalArgumentException.class);
        expected.expectMessage("name cannot be null or empty");

        new EsapiLoggerAdapter(mockLogger, null);
    }

    @Test
    public void constructorShouldRejectEmptyName() {
        expected.expect(IllegalArgumentException.class);
        expected.expectMessage("name cannot be null or empty");

        new EsapiLoggerAdapter(mockLogger, "");
    }

    @Test
    public void shouldHaveName() {
        assertThat(adapter.getName(), is("loggerName"));
    }

    @Test
    public void shouldHaveTraceEnabled() {
        assertTrue(adapter.isTraceEnabled());
    }

    @Test
    public void shouldNotHaveTraceEnabled() {
        when(mockLogger.isTraceEnabled()).thenReturn(false);
        assertFalse(adapter.isTraceEnabled());
    }

    @Test
    public void shouldTrace() {
        adapter.trace("this is a message");
        verify(mockLogger).trace(EVENT_UNSPECIFIED, "this is a message");
    }

    @Test
    public void shouldNotTraceWhenTraceIsDisabled() {
        when(mockLogger.isTraceEnabled()).thenReturn(false);
        adapter.trace("this is a message");
        verify(mockLogger).trace(any(Logger.EventType.class), anyString());
    }

    @Test
    public void shouldTraceWithFormatting_OneFormatParam() {
        adapter.trace("this is a {} message", "formatted");
        verify(mockLogger).trace(EVENT_UNSPECIFIED, "this is a formatted message", null);
    }

    @Test
    public void shouldNotTraceWithFormattingWhenTraceIsDisabled_OneFormatParam() {
        when(mockLogger.isTraceEnabled()).thenReturn(false);
        adapter.trace("this is a {} message", "formatted");
        verify(mockLogger, never()).trace(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldTraceWithThrowable_OneFormatParam() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.trace("this is a message", exception);
        verify(mockLogger).trace(EVENT_UNSPECIFIED, "this is a message", exception);
    }

    @Test
    public void shouldNotTraceWithThrowableWhenTraceIsDisabled_OneFormatParam() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        when(mockLogger.isTraceEnabled()).thenReturn(false);
        adapter.trace("this is a message", exception);
        verify(mockLogger, never()).trace(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldTraceWithFormatting_TwoFormatParams() {
        adapter.trace("this is a{} {} message", "nother", "formatted");
        verify(mockLogger).trace(EVENT_UNSPECIFIED, "this is another formatted message", null);
    }

    @Test
    public void shouldNotTraceWithFormattingWhenTraceIsDisabled_TwoFormatParams() {
        when(mockLogger.isTraceEnabled()).thenReturn(false);
        adapter.trace("this is a{} {} message", "nother", "formatted");
        verify(mockLogger, never()).trace(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldTraceWithThrowable_TwoFormatParams() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.trace("this is a {} message", "formatted", exception);
        verify(mockLogger).trace(EVENT_UNSPECIFIED, "this is a formatted message", exception);

        // Order shouldn't matter, so flip the arguments and check again.
        adapter.trace("this is a {} message", exception, "formatted");
        verify(mockLogger).trace(EVENT_UNSPECIFIED, "this is a formatted message", exception);
    }

    @Test
    public void shouldNotTraceWithThrowableWhenTraceIsDisabled_TwoFormatParams() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        when(mockLogger.isTraceEnabled()).thenReturn(false);
        adapter.trace("this is a {} message", "formatted", exception);
        verify(mockLogger, never()).trace(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldTraceWithFormatting_VarargFormatParams() {
        adapter.trace("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger).trace(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", null);
    }

    @Test
    public void shouldNotTraceWithFormattingWhenTraceIsDisabled_VarargFormatParams() {
        when(mockLogger.isTraceEnabled()).thenReturn(false);
        adapter.trace("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger, never()).trace(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldTraceWithThrowable_VarargFormatParams() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.trace("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters", exception);
        verify(mockLogger).trace(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", exception);

        // Order shouldn't matter, so throw the exception at the beginning of the array
        adapter.trace("this is a {} {} {} {} {}", exception, "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger).trace(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", exception);

        // One last order independence check:  throw the exception in the middle of the array
        adapter.trace("this is a {} {} {} {} {}", "message", "formatted", exception, "with", "vararg", "parameters");
        verify(mockLogger).trace(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", exception);
    }

    @Test
    public void shouldNotTraceWithThrowableWhenTraceIsDisabled_VarargFormatParams() {
        when(mockLogger.isTraceEnabled()).thenReturn(false);
        adapter.trace("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger, never()).trace(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldHaveDebugEnabled() {
        assertTrue(adapter.isDebugEnabled());
    }

    @Test
    public void shouldNotHaveDebugEnabled() {
        when(mockLogger.isDebugEnabled()).thenReturn(false);
        assertFalse(adapter.isDebugEnabled());
    }

    @Test
    public void shouldDebug() {
        adapter.debug("this is a message");
        verify(mockLogger).debug(EVENT_UNSPECIFIED, "this is a message");
    }

    @Test
    public void shouldNotDebugWhenDebugIsDisabled() {
        when(mockLogger.isDebugEnabled()).thenReturn(false);
        adapter.debug("this is a message");
        verify(mockLogger).debug(any(Logger.EventType.class), anyString());
    }

    @Test
    public void shouldDebugWithFormatting_OneFormatParam() {
        adapter.debug("this is a {} message", "formatted");
        verify(mockLogger).debug(EVENT_UNSPECIFIED, "this is a formatted message", null);
    }

    @Test
    public void shouldNotDebugWithFormattingWhenDebugIsDisabled_OneFormatParam() {
        when(mockLogger.isDebugEnabled()).thenReturn(false);
        adapter.debug("this is a {} message", "formatted");
        verify(mockLogger, never()).debug(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldDebugWithThrowable_OneFormatParam() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.debug("this is a message", exception);
        verify(mockLogger).debug(EVENT_UNSPECIFIED, "this is a message", exception);
    }

    @Test
    public void shouldNotDebugWithThrowableWhenDebugIsDisabled_OneFormatParam() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        when(mockLogger.isDebugEnabled()).thenReturn(false);
        adapter.debug("this is a message", exception);
        verify(mockLogger, never()).debug(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldDebugWithFormatting_TwoFormatParams() {
        adapter.debug("this is a{} {} message", "nother", "formatted");
        verify(mockLogger).debug(EVENT_UNSPECIFIED, "this is another formatted message", null);
    }

    @Test
    public void shouldNotDebugWithFormattingWhenDebugIsDisabled_TwoFormatParams() {
        when(mockLogger.isDebugEnabled()).thenReturn(false);
        adapter.debug("this is a{} {} message", "nother", "formatted");
        verify(mockLogger, never()).debug(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldDebugWithThrowable_TwoFormatParams() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.debug("this is a {} message", "formatted", exception);
        verify(mockLogger).debug(EVENT_UNSPECIFIED, "this is a formatted message", exception);

        // Order shouldn't matter, so flip the arguments and check again.
        adapter.debug("this is a {} message", exception, "formatted");
        verify(mockLogger).debug(EVENT_UNSPECIFIED, "this is a formatted message", exception);
    }

    @Test
    public void shouldNotDebugWithThrowableWhenDebugIsDisabled_TwoFormatParams() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        when(mockLogger.isDebugEnabled()).thenReturn(false);
        adapter.debug("this is a {} message", "formatted", exception);
        verify(mockLogger, never()).debug(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldDebugWithFormatting_VarargFormatParams() {
        adapter.debug("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger).debug(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", null);
    }

    @Test
    public void shouldNotDebugWithFormattingWhenDebugIsDisabled_VarargFormatParams() {
        when(mockLogger.isDebugEnabled()).thenReturn(false);
        adapter.debug("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger, never()).debug(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldDebugWithThrowable_VarargFormatParams() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.debug("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters", exception);
        verify(mockLogger).debug(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", exception);

        // Order shouldn't matter, so throw the exception at the beginning of the array
        adapter.debug("this is a {} {} {} {} {}", exception, "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger).debug(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", exception);

        // One last order independence check:  throw the exception in the middle of the array
        adapter.debug("this is a {} {} {} {} {}", "message", "formatted", exception, "with", "vararg", "parameters");
        verify(mockLogger).debug(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", exception);
    }

    @Test
    public void shouldNotDebugWithThrowableWhenDebugIsDisabled_VarargFormatParams() {
        when(mockLogger.isDebugEnabled()).thenReturn(false);
        adapter.debug("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger, never()).debug(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldHaveInfoEnabled() {
        assertTrue(adapter.isInfoEnabled());
    }

    @Test
    public void shouldNotHaveInfoEnabled() {
        when(mockLogger.isInfoEnabled()).thenReturn(false);
        assertFalse(adapter.isInfoEnabled());
    }

    @Test
    public void shouldInfo() {
        adapter.info("this is a message");
        verify(mockLogger).info(EVENT_UNSPECIFIED, "this is a message");
    }

    @Test
    public void shouldNotInfoWhenInfoIsDisabled() {
        when(mockLogger.isInfoEnabled()).thenReturn(false);
        adapter.info("this is a message");
        verify(mockLogger).info(any(Logger.EventType.class), anyString());
    }

    @Test
    public void shouldInfoWithFormatting_OneFormatParam() {
        adapter.info("this is a {} message", "formatted");
        verify(mockLogger).info(EVENT_UNSPECIFIED, "this is a formatted message", null);
    }

    @Test
    public void shouldNotInfoWithFormattingWhenInfoIsDisabled_OneFormatParam() {
        when(mockLogger.isInfoEnabled()).thenReturn(false);
        adapter.info("this is a {} message", "formatted");
        verify(mockLogger, never()).info(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldInfoWithThrowable_OneFormatParam() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.info("this is a message", exception);
        verify(mockLogger).info(EVENT_UNSPECIFIED, "this is a message", exception);
    }

    @Test
    public void shouldNotInfoWithThrowableWhenInfoIsDisabled_OneFormatParam() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        when(mockLogger.isInfoEnabled()).thenReturn(false);
        adapter.info("this is a message", exception);
        verify(mockLogger, never()).info(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldInfoWithFormatting_TwoFormatParams() {
        adapter.info("this is a{} {} message", "nother", "formatted");
        verify(mockLogger).info(EVENT_UNSPECIFIED, "this is another formatted message", null);
    }

    @Test
    public void shouldNotInfoWithFormattingWhenInfoIsDisabled_TwoFormatParams() {
        when(mockLogger.isInfoEnabled()).thenReturn(false);
        adapter.info("this is a{} {} message", "nother", "formatted");
        verify(mockLogger, never()).info(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldInfoWithThrowable_TwoFormatParams() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.info("this is a {} message", "formatted", exception);
        verify(mockLogger).info(EVENT_UNSPECIFIED, "this is a formatted message", exception);

        // Order shouldn't matter, so flip the arguments and check again.
        adapter.info("this is a {} message", exception, "formatted");
        verify(mockLogger).info(EVENT_UNSPECIFIED, "this is a formatted message", exception);
    }

    @Test
    public void shouldNotInfoWithThrowableWhenInfoIsDisabled_TwoFormatParams() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        when(mockLogger.isInfoEnabled()).thenReturn(false);
        adapter.info("this is a {} message", "formatted", exception);
        verify(mockLogger, never()).info(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldInfoWithFormatting_VarargFormatParams() {
        adapter.info("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger).info(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", null);
    }

    @Test
    public void shouldNotInfoWithFormattingWhenInfoIsDisabled_VarargFormatParams() {
        when(mockLogger.isInfoEnabled()).thenReturn(false);
        adapter.info("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger, never()).info(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldInfoWithThrowable_VarargFormatParams() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.info("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters", exception);
        verify(mockLogger).info(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", exception);

        // Order shouldn't matter, so throw the exception at the beginning of the array
        adapter.info("this is a {} {} {} {} {}", exception, "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger).info(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", exception);

        // One last order independence check:  throw the exception in the middle of the array
        adapter.info("this is a {} {} {} {} {}", "message", "formatted", exception, "with", "vararg", "parameters");
        verify(mockLogger).info(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", exception);
    }

    @Test
    public void shouldNotInfoWithThrowableWhenInfoIsDisabled_VarargFormatParams() {
        when(mockLogger.isInfoEnabled()).thenReturn(false);
        adapter.info("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger, never()).info(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldHaveWarnEnabled() {
        assertTrue(adapter.isWarnEnabled());
    }

    @Test
    public void shouldNotHaveWarnEnabled() {
        when(mockLogger.isWarningEnabled()).thenReturn(false);
        assertFalse(adapter.isWarnEnabled());
    }

    @Test
    public void shouldWarn() {
        adapter.warn("this is a message");
        verify(mockLogger).warning(EVENT_UNSPECIFIED, "this is a message");
    }

    @Test
    public void shouldNotWarnWhenWarnIsDisabled() {
        when(mockLogger.isWarningEnabled()).thenReturn(false);
        adapter.warn("this is a message");
        verify(mockLogger).warning(any(Logger.EventType.class), anyString());
    }

    @Test
    public void shouldWarnWithFormatting_OneFormatParam() {
        adapter.warn("this is a {} message", "formatted");
        verify(mockLogger).warning(EVENT_UNSPECIFIED, "this is a formatted message", null);
    }

    @Test
    public void shouldNotWarnWithFormattingWhenWarnIsDisabled_OneFormatParam() {
        when(mockLogger.isWarningEnabled()).thenReturn(false);
        adapter.warn("this is a {} message", "formatted");
        verify(mockLogger, never()).warning(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldWarnWithThrowable_OneFormatParam() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.warn("this is a message", exception);
        verify(mockLogger).warning(EVENT_UNSPECIFIED, "this is a message", exception);
    }

    @Test
    public void shouldNotWarnWithThrowableWhenWarnIsDisabled_OneFormatParam() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        when(mockLogger.isWarningEnabled()).thenReturn(false);
        adapter.warn("this is a message", exception);
        verify(mockLogger, never()).warning(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldWarnWithFormatting_TwoFormatParams() {
        adapter.warn("this is a{} {} message", "nother", "formatted");
        verify(mockLogger).warning(EVENT_UNSPECIFIED, "this is another formatted message", null);
    }

    @Test
    public void shouldNotWarnWithFormattingWhenWarnIsDisabled_TwoFormatParams() {
        when(mockLogger.isWarningEnabled()).thenReturn(false);
        adapter.warn("this is a{} {} message", "nother", "formatted");
        verify(mockLogger, never()).warning(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldWarnWithThrowable_TwoFormatParams() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.warn("this is a {} message", "formatted", exception);
        verify(mockLogger).warning(EVENT_UNSPECIFIED, "this is a formatted message", exception);

        // Order shouldn't matter, so flip the arguments and check again.
        adapter.warn("this is a {} message", exception, "formatted");
        verify(mockLogger).warning(EVENT_UNSPECIFIED, "this is a formatted message", exception);
    }

    @Test
    public void shouldNotWarnWithThrowableWhenWarnIsDisabled_TwoFormatParams() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        when(mockLogger.isWarningEnabled()).thenReturn(false);
        adapter.warn("this is a {} message", "formatted", exception);
        verify(mockLogger, never()).warning(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldWarnWithFormatting_VarargFormatParams() {
        adapter.warn("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger).warning(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", null);
    }

    @Test
    public void shouldNotWarnWithFormattingWhenWarnIsDisabled_VarargFormatParams() {
        when(mockLogger.isWarningEnabled()).thenReturn(false);
        adapter.warn("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger, never()).warning(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldWarnWithThrowable_VarargFormatParams() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.warn("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters", exception);
        verify(mockLogger).warning(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", exception);

        // Order shouldn't matter, so throw the exception at the beginning of the array
        adapter.warn("this is a {} {} {} {} {}", exception, "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger).warning(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", exception);

        // One last order independence check:  throw the exception in the middle of the array
        adapter.warn("this is a {} {} {} {} {}", "message", "formatted", exception, "with", "vararg", "parameters");
        verify(mockLogger).warning(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", exception);
    }

    @Test
    public void shouldNotWarnWithThrowableWhenWarnIsDisabled_VarargFormatParams() {
        when(mockLogger.isWarningEnabled()).thenReturn(false);
        adapter.warn("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger, never()).warning(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldHaveErrorEnabled() {
        assertTrue(adapter.isErrorEnabled());
    }

    @Test
    public void shouldNotHaveErrorEnabled() {
        when(mockLogger.isErrorEnabled()).thenReturn(false);
        assertFalse(adapter.isErrorEnabled());
    }

    @Test
    public void shouldError() {
        adapter.error("this is a message");
        verify(mockLogger).error(EVENT_UNSPECIFIED, "this is a message");
    }

    @Test
    public void shouldNotErrorWhenErrorIsDisabled() {
        when(mockLogger.isErrorEnabled()).thenReturn(false);
        adapter.error("this is a message");
        verify(mockLogger).error(any(Logger.EventType.class), anyString());
    }

    @Test
    public void shouldErrorWithFormatting_OneFormatParam() {
        adapter.error("this is a {} message", "formatted");
        verify(mockLogger).error(EVENT_UNSPECIFIED, "this is a formatted message", null);
    }

    @Test
    public void shouldNotErrorWithFormattingWhenErrorIsDisabled_OneFormatParam() {
        when(mockLogger.isErrorEnabled()).thenReturn(false);
        adapter.error("this is a {} message", "formatted");
        verify(mockLogger, never()).error(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldErrorWithThrowable_OneFormatParam() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.error("this is a message", exception);
        verify(mockLogger).error(EVENT_UNSPECIFIED, "this is a message", exception);
    }

    @Test
    public void shouldNotErrorWithThrowableWhenErrorIsDisabled_OneFormatParam() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        when(mockLogger.isErrorEnabled()).thenReturn(false);
        adapter.error("this is a message", exception);
        verify(mockLogger, never()).error(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldErrorWithFormatting_TwoFormatParams() {
        adapter.error("this is a{} {} message", "nother", "formatted");
        verify(mockLogger).error(EVENT_UNSPECIFIED, "this is another formatted message", null);
    }

    @Test
    public void shouldNotErrorWithFormattingWhenErrorIsDisabled_TwoFormatParams() {
        when(mockLogger.isErrorEnabled()).thenReturn(false);
        adapter.error("this is a{} {} message", "nother", "formatted");
        verify(mockLogger, never()).error(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldErrorWithThrowable_TwoFormatParams() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.error("this is a {} message", "formatted", exception);
        verify(mockLogger).error(EVENT_UNSPECIFIED, "this is a formatted message", exception);

        // Order shouldn't matter, so flip the arguments and check again.
        adapter.error("this is a {} message", exception, "formatted");
        verify(mockLogger).error(EVENT_UNSPECIFIED, "this is a formatted message", exception);
    }

    @Test
    public void shouldNotErrorWithThrowableWhenErrorIsDisabled_TwoFormatParams() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        when(mockLogger.isErrorEnabled()).thenReturn(false);
        adapter.error("this is a {} message", "formatted", exception);
        verify(mockLogger, never()).error(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldErrorWithFormatting_VarargFormatParams() {
        adapter.error("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger).error(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", null);
    }

    @Test
    public void shouldNotErrorWithFormattingWhenErrorIsDisabled_VarargFormatParams() {
        when(mockLogger.isErrorEnabled()).thenReturn(false);
        adapter.error("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger, never()).error(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldErrorWithThrowable_VarargFormatParams() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.error("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters", exception);
        verify(mockLogger).error(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", exception);

        // Order shouldn't matter, so throw the exception at the beginning of the array
        adapter.error("this is a {} {} {} {} {}", exception, "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger).error(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", exception);

        // One last order independence check:  throw the exception in the middle of the array
        adapter.error("this is a {} {} {} {} {}", "message", "formatted", exception, "with", "vararg", "parameters");
        verify(mockLogger).error(EVENT_UNSPECIFIED, "this is a message formatted with vararg parameters", exception);
    }

    @Test
    public void shouldNotErrorWithThrowableWhenErrorIsDisabled_VarargFormatParams() {
        when(mockLogger.isErrorEnabled()).thenReturn(false);
        adapter.error("this is a {} {} {} {} {}", "message", "formatted", "with", "vararg", "parameters");
        verify(mockLogger, never()).error(any(Logger.EventType.class), anyString(), any(Throwable.class));
    }

    @Test
    public void shouldLogAtTrace() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.log(mock(Marker.class), "fqcn", TRACE_INT, "message", new String[]{"arg", "array"}, exception);
        verify(mockLogger).trace(EVENT_UNSPECIFIED, "message", exception);
    }

    @Test
    public void shouldLogAtDebug() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.log(mock(Marker.class), "fqcn", LocationAwareLogger.DEBUG_INT, "message", new String[]{"arg", "array"}, exception);
        verify(mockLogger).debug(EVENT_UNSPECIFIED, "message", exception);
    }

    @Test
    public void shouldLogAtInfo() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.log(mock(Marker.class), "fqcn", INFO_INT, "message", new String[]{"arg", "array"}, exception);
        verify(mockLogger).info(EVENT_UNSPECIFIED, "message", exception);
    }

    @Test
    public void shouldLogAtWarning() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.log(mock(Marker.class), "fqcn", WARN_INT, "message", new String[]{"arg", "array"}, exception);
        verify(mockLogger).warning(EVENT_UNSPECIFIED, "message", exception);
    }

    @Test
    public void shouldLogAtError() {
        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.log(mock(Marker.class), "fqcn", ERROR_INT, "message", new String[]{"arg", "array"}, exception);
        verify(mockLogger).error(EVENT_UNSPECIFIED, "message", exception);
    }

    @Test
    public void shouldRejectUnknownLogLevel() {
        expected.expect(IllegalArgumentException.class);
        expected.expectMessage("Unknown log level number: " + INVALID_LOG_LEVEL);

        IllegalArgumentException exception = new IllegalArgumentException("test exception");
        adapter.log(mock(Marker.class), "fqcn", INVALID_LOG_LEVEL, "message", new String[]{"arg", "array"}, exception);
    }
}