/*
 * Copyright (c) 2017 Billy Flanagan
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package geekpreserve.utility.logging;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

public class EsapiLoggerFactoryTests {
    @Rule
    public ExpectedException expected = ExpectedException.none();

    private final EsapiLoggerFactory factory = new EsapiLoggerFactory();

    @Test
    public void shouldGetALogger() {
        assertNotNull(factory.getLogger("myAwesomeLogger"));
    }

    @Test
    public void shouldCacheLoggerInstances() {
        assertSame(factory.getLogger("myAwesomeLogger"), factory.getLogger("myAwesomeLogger"));
    }

    @Test
    public void shouldRejectNullLoggerName() {
        expected.expect(IllegalArgumentException.class);
        expected.expectMessage("name cannot be null or empty");

        factory.getLogger(null);
    }

    @Test
    public void shouldRejectEmptyLoggerName() {
        expected.expect(IllegalArgumentException.class);
        expected.expectMessage("name cannot be null or empty");

        factory.getLogger("");
    }

}